package com.panisa.week10;

/**
 * Hello world!
 *
 */
public class App {
    public static void main( String[] args ) {
        Rectangle rec = new Rectangle(5, 3);
        System.out.println(rec.toString());
        System.out.println(rec.calArea());
        System.out.println(rec.calPerimater());

        Rectangle rec2 = new Rectangle(2, 2);
        System.out.println(rec2.toString());
        System.out.println(rec2.calArea());
        System.out.println(rec2.calPerimater());

        Circle circle1 = new Circle(2);
        System.out.println(circle1);
        System.out.printf("%.3f\n", circle1.calArea());
        System.out.printf("%.3f\n", circle1.calPerimater());

        Circle circle2 = new Circle(3);
        System.out.println(circle2);
        System.out.printf("%.3f\n", circle2.calArea());
        System.out.printf("%.3f\n", circle2.calPerimater());

        Triangle tri = new Triangle(5, 5, 6);
        System.out.println(tri);
        System.out.println(tri.calArea());
        System.out.println(tri.calPerimater());

    }
}
